<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Models\Employee;
class EmployeeController extends Controller
{
    public function index(){
        $data=Employee::all();
           return view('index',compact('data'));
       }
   
       public function back(){
        return redirect ('/');
       }
       
       public function create(){
        return view('create');
       }
   
       public function insert(Request $request){
        $data=new Employee();
           $data->nama=$request->get('nama');
           $data->atasan_id=$request->get('atasan_id');
           $data->company_id=$request->get('company_id');
           $data->save();
        return redirect ('/');
       }
   
       public function delete($id){
        $data=Employee::find($id);
           $data->delete();
           return back();
       }
   
       public function edit($id){
        $data=Employee::find($id);
        return view('edit',compact('data'));
       }
   
       public function update(Request $request, $id){     
        $data = Employee::findOrFail($id);
           $data->nama=$request->get('nama');
           $data->atasan_id=$request->get('atasan_id');
           $data->company_id=$request->get('company_id');
           $data->save();
        return redirect ('/')->with('alert-success','Data berhasil Diubah.');
       }
   
       public function read($id){
        $data=Employee::find($id);
        return view('read',compact('data'));
       }
}
