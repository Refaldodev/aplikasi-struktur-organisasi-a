<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CompanyController::class, 'index']);
Route::get('back', [CompanyController::class, 'back']);
Route::get('create', [CompanyController::class, 'create']);
Route::post('insert', [CompanyController::class, 'insert']);
Route::get('delete/{id}', [CompanyController::class, 'delete']);
Route::get('edit/{id}', [CompanyController::class, 'edit']);
Route::post('update/{id}', [CompanyController::class, 'update']);
Route::get('read/{id}', [CompanyController::class, 'read']);
