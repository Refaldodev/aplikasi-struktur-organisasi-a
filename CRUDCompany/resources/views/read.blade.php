<!DOCTYPE html>
<html>
<head>
    <title>CRUD Company</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>CRUD Company</h4>
            </div>
            <div class="panel-body">
                    {{-- <input type="hidden" name="id" id="id" value="{{$data->id}}"> --}}
                    <div class="form-group">
                        <label for="nama">nama</label>
                        <input type="text" name="nama" id="nama" value="{{$data->nama}}" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="alamat">alamat</label>
                        <input type="text" name="alamat" id="alamat" value="{{$data->alamat}}" class="form-control" readonly>
                    </div>                    
                    <form action="{{url('back')}}" method="get">
                        <div class="form-group">
                            <input type="submit" value="Back" class="btn btn-success">
                        </div>
                    </form>
            </div>
        </div>
    </div>
</body>
</html>